from keras import Input, Model
from keras.callbacks import TensorBoard
from keras.layers import Dense, np
from keras.optimizers import SGD

BATCH_SIZE = 1

X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
Y = np.array([[0], [1], [1], [0]])


def build_model():
    input = Input(shape=(2,))
    hidden = Dense(2, activation='tanh')(input)
    output = Dense(1, activation='sigmoid')(hidden)
    model = Model(inputs=[input], outputs=[output])

    sgd = SGD(lr=0.1)
    model.compile(loss='binary_crossentropy', optimizer=sgd)
    return model


if __name__ == '__main__':
    model = build_model()

    model.fit(X, Y, batch_size=BATCH_SIZE, epochs=1000, verbose=False,
              callbacks=[TensorBoard(log_dir='data/xor/log', histogram_freq=0,
                                     batch_size=BATCH_SIZE, write_graph=True,
                                     write_grads=False, write_images=False)])
    print(model.predict(X))
