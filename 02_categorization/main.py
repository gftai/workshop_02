import os
import time

import PIL.Image
import PIL.ImageOps
import numpy as np
import torch
import torch.nn.functional as F
import torch.utils.data
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from torch import nn

EPOCHS = 10
BATCH_SIZE = 16
DATA_SET_SIZE = 2000
SQUARE_DIM = 400

DIR_OUT_IMAGES = "out_images"
IMG_SRC_FILE = "square.png"


class Net(nn.Module):

    def __init__(self, hidden_size: int):
        super(Net, self).__init__()

        self.activation = F.relu
        self.hidden_size = hidden_size

        self.hidden01 = nn.Linear(2, self.hidden_size)
        self.hidden02 = nn.Linear(self.hidden_size, self.hidden_size)
        # self.hidden03 = nn.Linear(self.hidden_size, self.hidden_size)
        self.output = nn.Linear(self.hidden_size, 1)

    def forward(self, x):
        x = self.activation(self.hidden01(x))
        x = self.activation(self.hidden02(x))
        # x = self.activation(self.hidden03(x))
        x = F.sigmoid(self.output(x))
        return x


_X = np.arange(SQUARE_DIM)
_Y = np.arange(SQUARE_DIM)
_D = np.transpose([np.tile(_X, len(_Y)), np.repeat(_Y, len(_X))]) - (SQUARE_DIM / 2)

IMG_SRC = PIL.ImageOps.colorize(PIL.Image.open(IMG_SRC_FILE).convert('L'), (0, 0, 0), (255, 255, 255))


def gen_train_data_image(out_img_dir: str, data: np.ndarray, src_img_data):
    img_out = PIL.Image.new('RGB', (SQUARE_DIM, SQUARE_DIM))
    img_out.paste((0, 0, 0), [0, 0, SQUARE_DIM, SQUARE_DIM])
    img_data = img_out.load()
    for x, y in data:
        x = int(x)
        y = int(y)
        img_data[x, y] = src_img_data[x, y]
    img_out.save(os.path.join(out_img_dir, "training_data.png"))


def gen_image(net: torch.nn.Module, epoch: int, out_img_dir: str, device):
    image_data = np.around(net.eval()(torch.from_numpy(_D).float().to(device)).cpu().detach().numpy())
    img_out = PIL.Image.new('L', (SQUARE_DIM, SQUARE_DIM))
    img_out.putdata(image_data * 255)  # Scale from [0, 1] to 0-255 grey-scale
    img_out = PIL.ImageOps.colorize(img_out, (0, 0, 0), (0, 255, 255))
    return PIL.Image.blend(img_out, IMG_SRC, 0.6)#.save(os.path.join(out_img_dir, "out_{0:04d}.png".format(epoch)))


METRICS_HISTORY = {
    "test": [],
    "train": []
}


def print_metrics(net: torch.nn.Module, x_train, x_test, y_train, y_test, device, epoch: int, span,
                  running_loss) -> None:
    # Stateful
    def apr(name: str, y_true, y_pred) -> str:
        scores = [f(y_true, y_pred) * 100 for f in (accuracy_score, precision_score, recall_score)]
        METRICS_HISTORY[name].append(scores)
        return name + " APR: {:.2f}%/{:.2f}%/{:.2f}%".format(*scores)

    y_res_train = np.around(net.eval()(x_train.to(device)).cpu().detach().numpy())
    y_res_test = np.around(net.eval()(x_test.to(device)).cpu().detach().numpy())

    np_y_train = y_train.detach().numpy()
    np_y_test = np.array(y_test)

    apr_train = apr("train", np_y_train, y_res_train)
    apr_test = apr("test", np_y_test, y_res_test)

    print(
        "Epoch: {} took {:.2f}s, loss: {:.4f}, {}, {}".format(epoch, span,
                                                              running_loss / x_train.size()[0], apr_train,
                                                              apr_test))


def main(cuda: bool, out_img_dir, cnt_hiddent_neurons: int):
    device = torch.device("cuda" if cuda else "cpu")

    data = PIL.Image.open(IMG_SRC_FILE).convert("1").load()

    # for x,y in np.floor(np.random.uniform(0, SQUARE_DIM, (1000,2))): data[x,y] = 255
    image_samples = np.floor(np.random.uniform(0, SQUARE_DIM, (DATA_SET_SIZE, 2)))
    gen_train_data_image(out_img_dir, image_samples, data)
    TRAIN_DATA = [(x, y, data[x, y] / 255) for x, y in image_samples]
    # noinspection PyCallingNonCallable
    X = [torch.tensor((x, y)) - (SQUARE_DIM / 2) for x, y, v in TRAIN_DATA]
    Y = [v for _, _, v in TRAIN_DATA]
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=42, stratify=Y)

    x_train = torch.stack(x_train)
    x_test = torch.stack(x_test)
    y_train = torch.FloatTensor(y_train)

    data_set = torch.utils.data.TensorDataset(x_train, y_train)
    train_loader = torch.utils.data.DataLoader(data_set, shuffle=True, batch_size=BATCH_SIZE)

    net = Net(cnt_hiddent_neurons).to(device)
    criterion = nn.BCELoss()
    # optimizer = torch.optim.SGD(net.parameters(), lr=0.00001, momentum=0.9)
    optimizer = torch.optim.Adam(net.parameters())

    images = []

    for epoch in range(EPOCHS):
        tm_start = time.time()
        running_loss = 0.0
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)

            outputs = net.train()(inputs)
            loss = criterion(outputs, labels)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.data.item()

        print_metrics(net, x_train, x_test, y_train, y_test, device, epoch, time.time() - tm_start, running_loss)
        images.append(gen_image(net, epoch, out_img_dir, device))

        images[0].save("out.gif", save_all = True, append_images = images[1:])

def plot_metrics():
    fig = {
        'data': [
            {
                'name': 'Accuracy',
                'mode': 'lines+markers',
                'x': list(range(len(METRICS_HISTORY["test"]))),
                'y': [m[0] for m in METRICS_HISTORY["test"]]
            },
            {
                'name': 'Precision',
                'mode': 'lines+markers',
                'x': list(range(len(METRICS_HISTORY["test"]))),
                'y': [m[1] for m in METRICS_HISTORY["test"]]
            },
            {
                'name': 'Recall',
                'mode': 'lines+markers',
                'x': list(range(len(METRICS_HISTORY["test"]))),
                'y': [m[2] for m in METRICS_HISTORY["test"]]
            }
        ]
    }

    import plotly
    plotly.offline.plot(fig, filename="plot.html", auto_open=False)


if __name__ == '__main__':
    dir = os.path.dirname(os.path.realpath(__file__))
    img_dir = os.path.join(dir, DIR_OUT_IMAGES)
    if not os.path.exists(img_dir):
        os.makedirs(img_dir)
    main(False, img_dir, 32)
    plot_metrics()
