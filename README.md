# README #
## Todo ##

* Port XOR to PyTorch
* Introduce AND in addition to XOR
* ~~Fix environment.yml as it is currently broken~~
* https://stackoverflow.com/questions/20924085/python-conversion-between-coordinates

## Instructions ##

### Preparations ###

* Download **Anaconda 5.1** distribution for your OS (**Python 3.6** version) from: https://www.anaconda.com/download/
* Navigate to the Anaconda installation folder / Scripts and open shell from there
* Do `conda env create --file <PATH_TO_THE_CHECKED_OUT_PROJECT>\environment.yml`
* To remove the env, do: `conda env remove -n aiworkshop`

### Libraries we are using ###

* Programming language: Python 3
* Library for neural networks: PyTorch
* Image processing: Pillow 
* scikit-learn for generic ML tooling, like cross validation, precision/recall, etc.